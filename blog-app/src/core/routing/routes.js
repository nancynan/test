import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Blog from '../../features/blog/blog-component';
import PostView from '../../features/blog/post-view/post-view-component';

const Routes = () => {
  return (
    <Switch>
      <Redirect
        exact
        from='/'
        to='/posts'
      />
      <Route
        component={PostView}
        path='/posts/:postId'
      />
      <Route
        component={Blog}
        path='/posts'
      />
    </Switch>
  );
};

export default Routes;
