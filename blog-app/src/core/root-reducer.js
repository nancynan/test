import { combineReducers } from 'redux';
import blog from '../features/blog/reducer';

export default combineReducers({
  blog,
});
