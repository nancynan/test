import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Post from './post/post-component';

const PostsContainer = styled.div`
  display: flex;
  flex-flow: row nowrap;
`;

const PostsList = ({ posts }) => {
  return (
    <PostsContainer>
      {
        posts.map(post => {
          return (
            <Post
              key={post.id}
              post={post}
            />
          );
        })
      }
    </PostsContainer>
  );
};

export default PostsList;

PostsList.propTypes = {
  posts: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      body: PropTypes.string,
    })
  ),
};
