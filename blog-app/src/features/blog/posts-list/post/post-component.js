import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const Title = styled.h3`
  color: #522c90;
  font: 1.3rem sans-serif;
  margin: 0.5em 0 0 0;
`;
const PostData = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-evenly;
  width: 30%;
  margin: 1em;
  padding: 0.5em;
  background-color: #f7f7f7;
`;
const PostDescriptionBlock = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  align-items: flex-end;
`;
const PostDescription = styled.p`
  width: 55%;
  color: #76767d;
  font: 1rem sans-serif;
  margin: 1em 0 0 0;
`;
const LinkToPost = styled(Link)`
  color: #735a9c;
  text-decoration: none;
  font: 1rem sans-serif;
  margin: 0;

  :hover {
    color: #f1598d;
  }
`;

const Post = ({ post }) => {
  const wordsQuantity = 10;

  return (
    <PostData>
      <Title>{post.title}</Title>
      <PostDescriptionBlock>
        <PostDescription>
          {post.body.split(' ').slice(0, wordsQuantity).join(' ')}...
        </PostDescription>
        <LinkToPost to={`/posts/${post.id}`}>Read more</LinkToPost>
      </PostDescriptionBlock>
    </PostData>
  );
};

export default Post;

Post.propTypes = {
  post: PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    body: PropTypes.string,
  }),
};
