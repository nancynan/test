export const getCurrentComments = (comments, id) => {
  const current = [];
  comments.forEach(el => {
    if (el.postId === id) {
      current.push(el);
    }
  });
  return current;
};
