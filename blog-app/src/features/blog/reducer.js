import { RECEIVE_POSTS, RECEIVE_POST, RECEIVE_COMMENTS, UPDATE_COMMENTS } from './types';

export const initialStore = {
  posts: [],
  comments: [],
};

export default function blogReducer(store = initialStore, action ) {
  switch (action.type) {
    case RECEIVE_POSTS:
      return {
        ...store,
        posts: action.payload,
      };

    case RECEIVE_POST:
      return {
        ...store,
        post: action.payload,
      };

    case RECEIVE_COMMENTS:
      return {
        ...store,
        comments: action.payload,
      };

    case UPDATE_COMMENTS:
      return {
        ...store,
        newComments: store.comments.push(action.payload),
      };

    default:
      return store;
  }
}
