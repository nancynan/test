import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import styled from 'styled-components';
import { fetchPost, fetchComments } from '../actions';
import PostComments from './post-comments/post-comments-component';
import { getCurrentComments } from '../selectors';

const PostContainer = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-evenly;
  margin: 1em;
  padding: 0.5em;
  background-color: #f7f7f7;
`;
const Title = styled.h1`
  color: #522c90;
  font: 1.3rem sans-serif;
`;
const Description = styled.p`
  color: #76767d;
  font: 1rem sans-serif;
  margin: 1em 0 0 0;
`;

class PostView extends Component {
  componentDidMount() {
    this.props.fetchPost(Number(this.props.match.params.postId));
    this.props.fetchComments();
  }

  render() {
    const { post, comments } = this.props;

    return post ? (
      <React.Fragment>
        <PostContainer>
          <Title>{post.title}</Title>
          <Description>{post.body}</Description>
        </PostContainer>
        <PostComments comments={comments} />
      </React.Fragment>
    ) : null;
  }
}

const mapStateToProps = (state, props) => ({
  post: state.blog.post,
  comments: getCurrentComments(
    state.blog.comments,
    Number(props.match.params.postId),
  ),
});

const mapDispatchToProps = (dispatch) => ({
  fetchPost: (id) => dispatch(fetchPost(id)),
  fetchComments: () => dispatch(fetchComments()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PostView);

PostView.propTypes = {
  post: PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    body: PropTypes.string,
  }),
};
