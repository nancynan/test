import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { fetchUpdateComments } from '../../actions';

const CommentsContainer = styled.div`
  width: 70%;
  margin: 0 auto;
`;
const Title = styled.h3`
  color: #7953bd;
  font: 1.2rem sans-serif;
  text-align: center;
`;
const Comment = styled.p`
  color: #6d6d75;
  font: 0.9rem sans-serif;
  border-left: 2px solid #7953bd;
  padding-left: 1em;
  line-height: 1.5em;
`;
const WithoutComments = styled.p`
  color: #7953bd;
  font: 1.2rem sans-serif;
  text-align: center;
`;
const Input = styled.input`
  width: 70%;
  height: 1.5em;
`;

const PostComments = ({ comments, post, handleUpdateComments }) => {
  const newComment = {};
  const handleValue = (event) => {
    newComment.postId = post.id;
    newComment.body = event.target.value;
  };
  const handleKeyPress = (event) => {
    if (event.key === 'Enter' && !!newComment.body) {
      handleUpdateComments(newComment);
    }
  };
  return comments.length > 0 ?
    (
      <CommentsContainer>
        <Title>Comments</Title>
        <Input
          onChange={handleValue}
          onKeyPress={handleKeyPress}
          placeholder={'Type something and press Enter...'}
          type={'text'}
        />
        {comments.map(comment => <Comment key={comment.id}>{comment.body}</Comment>)}
      </CommentsContainer>
    )
    : <CommentsContainer>
      <WithoutComments>Be the first. Leave your comment.</WithoutComments>
      <Input
        onChange={handleValue}
        onKeyPress={handleKeyPress}
        placeholder={'Type something and press Enter...'}
        type={'text'}
      />
    </CommentsContainer>;
};

const mapStateToProps = (state) => ({
  updatedComments: state.blog.newComments,
  post: state.blog.post,
});

const mapDispatchToProps = (dispatch) => ({
  handleUpdateComments: (comment) => dispatch(fetchUpdateComments(comment)),
});


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PostComments);

PostComments.propTypes = {
  post: PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    body: PropTypes.string,
  }),
  comments: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      postId: PropTypes.number,
      body: PropTypes.string,
    })
  ),
  handleUpdateComments: PropTypes.func,
};
