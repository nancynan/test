import { RECEIVE_POSTS, RECEIVE_POST, RECEIVE_COMMENTS, UPDATE_COMMENTS } from './types';

export const fetchPosts = () => {
  return (dispatch, state, api) => {
    api('posts', 'get')
      .then(response => {
        dispatch(receivePosts(response.data));
      });
  };
};

export const receivePosts = payload => ({
  type: RECEIVE_POSTS,
  payload,
});

export const fetchPost = (id) => {
  return (dispatch, state, api) => {
    api(`posts/${id}`, 'get')
      .then(response => {
        dispatch(receivePost(response.data));
      });
  };
};

export const receivePost = payload => ({
  type: RECEIVE_POST,
  payload,
});

export const fetchComments = () => {
  return (dispatch, state, api) => {
    api(`comments`, 'get')
      .then(response => {
        dispatch(receiveComments(response.data));
      });
  };
};

export const receiveComments = payload => ({
  type: RECEIVE_COMMENTS,
  payload,
});

export const fetchUpdateComments = (comment) => {
  return (dispatch, state, api) => {
    api(`comments`, 'post', comment)
      .then(response => {
        comment.id = response.data.id;
        dispatch(updateComments(comment));
      });
  };
};

export const updateComments = payload => ({
  type: UPDATE_COMMENTS,
  payload,
});
