import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { fetchPosts } from './actions';
import PostList from './posts-list/posts-list-component';

const Title = styled.h1`
  text-align: center;
  font: 2.2rem sans-serif;
`;

export class Blog extends Component {
  componentDidMount() {
    this.props.fetchPosts();
  }

  render() {
    const { posts } = this.props;

    return (
      <React.Fragment>
        <Title>Blog Posts</Title>
        <PostList posts={posts} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  posts: state.blog.posts,
});

const mapDispatchToProps = (dispatch) => ({
  fetchPosts: () => dispatch(fetchPosts()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Blog);

Blog.propTypes = {
  posts: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      body: PropTypes.string,
    })
  ),
};
